<?php
namespace Maowenke\PersonalPaymentSdk;
class Config
{
    protected $config = [
    ];
    protected $message = '';
    protected $curl = '';

    /**
     * Wecate constructor.
     * @param array $config
     */
    public function __construct(array $config=[]){
        if(!empty($config)){
            foreach ($config as $key=>$value){
                $this->config[strtolower($key)] = $value;
            }
//            $this->config = $config;
        }
        $this->curl = new Curl();
    }

    /**设置配置信息
     * @param array $config
     */
    public function setConfig(array $config=[]){
        if(!empty($config)){
            foreach ($config as $key=>$value){
                $this->config[strtolower($key)] = $value;
            }
//            $this->config = $config;
        }
        return $this;
    }

    /**获取配置
     * @return array
     */
    public function getCofnig(string $str=''){
        if(empty($str)){
            return $this->config;
        }else{
            $str = strtolower($str);
            if($str=='appid'||$str=='app_id'){
                return $this->config['appid']??$this->config['app_id'];
            }else{
                return $this->config[$str];
            }
        }
    }

    /**验证数据
     * @return bool
     */
    public function verification($str='appid',$return=true){
        if($str=='appid'||$str=='app_id'){
            if(!isset($this->config['appid'])&&!isset($this->config['app_id'])){
                if($return){
                    $this->message = '请设置appid';
                    return false;
                }else{
                    throw new \Exception('请设置appid');
                }
            }
            if(empty($this->config['appid'])&&empty($this->config['app_id'])){
                if($return){
                    $this->message = 'appid不能为空';
                    return false;
                }else{
                    throw new \Exception('appid不能为空');
                }
            }
        }elseif($str=='secret'){
            if(!isset($this->config['secret'])&&!isset($this->config['secret'])){
                if($return){
                    $this->message = '请设置secret';
                    return false;
                }else{
                    throw new \Exception('请设置secret');
                }
            }
            if(empty($this->config['secret'])&&empty($this->config['secret'])){
                if($return){
                    $this->message = 'secret不能为空';
                    return false;
                }else{
                    throw new \Exception('secret不能为空');
                }
            }
        }else{
            if(is_array($str)){
                foreach ($str as $value){
                    if(empty($this->config[$value])){
                        if($return){
                            $this->message =  $value.'不能为空';
                            return false;
                        }else{
                            throw new \Exception( $value.'不能为空');
                        }
                    }
                }
            }else{
                if(empty($this->config[$str])){
                    if($return){
                        $this->message = $str.'不能为空';
                        return false;
                    }else{
                        throw new \Exception($str.'不能为空');
                    }
                }
            }
        }
        return true;
    }

    /**检查数据
     * @param array $key
     * @param array $data
     * @param $return
     * @return bool
     * @throws \Exception
     */
    public function verificationData($key=[],$data=[],$return){
        foreach ($key as $value){
            if(!(isset($data[$value])&&$data[$value])){
                if($return){
                    $this->message = $value.'必须传入';
                    return false;
                }else{
                    throw new \Exception($value.'必须传入');
                }
            }
        }
        return true;
    }

    /**获取信息
     * @return string
     */
    public function getMessage(){
        return $this->message;
    }
    /**获取当前用户的ip
     * @return array|false|mixed|string
     */
    protected function getIp(){
        if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown"))
            $ip = getenv("REMOTE_ADDR");
        else if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown"))
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = "unknown";
        return($ip);
    }
}