<?php
$config = [
    'appid'=>'**********',
    'secret'=>'**********',
];
$obj = \Maowenke\PersonalPaymentSdk\Wecate::make('applet',$config);
//或者
$obj = new \Maowenke\PersonalPaymentSdk\applet\WecateApplet($config);
//获取openid
$code = '******';
$array = $obj->getOpenId($code,false);
//两种方法都可以获取
$array = $obj->jscode2session($code);

//获取opeid
$openid = $obj->getOpenId($code,'openid');
$openid = $array['openid'];
//获取session_key
$session_key = $obj->getOpenId($code,'session_key');
$session_key = $array['session_key'];
//解密数据
$encryptedData = '************';
$iv = '************';
$data = $obj->decryptData($session_key,$encryptedData,$iv);

