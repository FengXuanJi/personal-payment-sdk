<?php
namespace Maowenke\PersonalPaymentSdk\applet;
use Maowenke\PersonalPaymentSdk\Wecate;

class WecateApplet extends Wecate
{
    protected $url = 'https://api.weixin.qq.com/';

    /**获取openid
     * @param string $code
     * @return bool|null
     */
    public function getOpenId(string $code='',$key='openid'){
        if(empty($code)){
            return false;
        }
        $bool = $this->verification();
        if(!$bool){
            return $bool;
        }
        $url = $this->url."sns/jscode2session?appid={$this->getCofnig('appid')}&secret={$this->getCofnig('secret')}&js_code=$code&grant_type=authorization_code";
        $array = $this->curl->http_requests($url,[],[],'GET');
        if(is_array($array)){
            if($key==false){
                return $array;
            }
            return $array[$key]??'';
        }else{
            $this->message = $array;
            return false;
        }
    }

    /**获取session
     * @param string $code
     * @return array|bool|string|null openid|session_key
     */
    public function jscode2session(string $code){
        if(empty($code)){
            return false;
        }
        $bool = $this->verification();
        if(!$bool){
            return $bool;
        }
        $url = $this->url."sns/jscode2session?appid={$this->getCofnig('appid')}&secret={$this->getCofnig('secret')}&js_code=$code&grant_type=authorization_code";
        $array = $this->curl->http_requests($url,[],[],'GET');
        if(is_array($array)){
            if(isset($array['errcode'])){
                $this->message = 'errmsg';
                return false;
            }
            return $array;
        }else{
            $this->message = $array;
            return false;
        }
    }

    /**
     * @param $session_key
     * @param $encryptedData
     * @param $iv
     * @return false|string
     */
    public function decryptData( $session_key,$encryptedData, $iv )
    {
        if (strlen($session_key) != 24) {
            return false;
        }
        $aesKey=base64_decode($session_key);
        if (strlen($iv) != 24) {
            return false;
        }
        $aesIV=base64_decode($iv);
        $aesCipher=base64_decode($encryptedData);
        $result=openssl_decrypt( $aesCipher, "AES-128-CBC", $aesKey, 1, $aesIV);
        $dataObj=json_decode( $result );
        if( $dataObj  == NULL )
        {
            $this->message = '解密失败';
            return false;
        }
        if( $dataObj->watermark->appid != $this->getCofnig('appid'))
        {
            $this->message = 'appid不正确';
            return false;
        }
        $data = json_decode($result,true);
        if(isset($data['errcode'])){
            $this->message = $data['errmsg']??'未返回信息';
            return false;
        }
        if(isset($data['phoneNumber'])||isset($data['purePhoneNumber'])){
            $data['phone'] = $data['phoneNumber']??$data['purePhoneNumber'];
        }
        return $data;
    }
}