<?php
namespace Maowenke\PersonalPaymentSdk\payment;
use Maowenke\PersonalPaymentSdk\Wecate;

class WecatePayment extends Wecate
{
    protected $url = 'https://api.mch.weixin.qq.com/';

    /**统一下单接口
     * @param $data
     * @return array|bool|mixed|string
     * @throws \Exception
     */
    public function create_orders($data){
        //检查必须得配置
        $bool = $this->verification(['appid','mch_id','mch_key','trade_type','notify_url']);
        if($bool===false){
            return false;
        }
        $bool = $this->verificationData(['body','total_fee'],$data);
        if($bool===false){
            return false;
        }
        //
        if(!isset($data['appid'])){
            $data['appid'] = $this->getCofnig('appid');
        }
        if(!isset($data['secret'])){
            $data['secret'] = $this->getCofnig('secret');
        }
        $data['mch_id'] = $this->getCofnig('mch_id');
        if(!(isset($data['nonce_str'])&&$data['nonce_str'])){
            $data['nonce_str'] =uniqid();
        }
        $data['sign_type'] = $this->getCofnig('sign_type');
        $data['body'] = $data['body'];
        if(!(isset($data['out_trade_no'])&&$data['out_trade_no'])){
            $data['out_trade_no'] = uniqid();
        }
        $data['total_fee'] = intval($data['total_fee']*100);
        if(!(isset($data['spbill_create_ip'])&&$data['spbill_create_ip'])){
            $data['spbill_create_ip'] = $this->getIp();
        }
        $data['time_expire'] = date('YmdHis',(time()+7200));
        $data['notify_url'] = $this->getCofnig('notify_url');
        $data['trade_type'] = $this->getCofnig('trade_type');
        $data['sign'] = $this->getSign($data);
//        echo '<pre>';
//        print_r($data);
//        exit;
        $this->newdata = $data;
        $data = $this->array_to_xml($data);
        if($data===false){
            $this->message = '数组转xml失败';
            return false;
        }
        $bool = $this->curl->xml_http($this->url.'pay/unifiedorder',$data);
        $this->rult = $bool;
        if(!is_array($bool)){
            $bool = $this->xml($bool);
        }
        return $this->getrult();
    }
    /**查询订单
     * @param string $order_number
     * @return bool|string
     */
    public function query_payment($order_number=''){
        $bool = $this->verification(['appid','mch_id','mch_key']);
        if($bool===false){
            return false;
        }
        if(!$order_number){
            $this->message = '没有设置订单号';
            return false;
        }
        $newdata['appid'] = $this->config['appid'];
        $newdata['mch_id'] = $this->config['mch_id'];
        $newdata['out_trade_no'] = $order_number;
        $newdata['nonce_str'] = uniqid();
        $newdata['sign'] = $this->getSign($newdata);
        $this->newdata = $newdata;
        $newdata = $this->array_to_xml($newdata);
        $url = $this->url.'pay/orderquery';
        $res = $this->curl->http_requests($url,$newdata,[],'POST');
        $this->rult = $res;
        if(!is_array($res)){
            $res = $this->xml($res);
        }
        return $res;
    }

    /**
     * @return array|false
     */
    public function callBack()
    {
        $testxml = file_get_contents("php://input");
        $testxml = $this->xml($testxml);
       if(!$testxml){
           $this->message = '解析xml失败';
           return false;
       }
       return $testxml;
    }

}