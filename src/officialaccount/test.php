<?php
$config = [
    'appid'=>'**********',
    'secret'=>'**********',
    'redirect_uri'=>'**********',
    'scope'=>'snsapi_base|snsapi_userinfo',
    'response_type'=>'code',
    'state'=>'123456',
];
$obj = new \Maowenke\PersonalPaymentSdk\officialaccount\PublicNumber();
//获取code的访问地址
$url = $obj->getCodeUrl(false);
//参数true为直接跳转
//获取用户的openid|access_token
$code = 'XXXXXXXX';
$array = $obj->getOauth2($code);
$openid = $array['openid'];
$access_token = $array['access_token'];
//获取用户详情
$array = $obj->getUserInfo($access_token,$openid);
//return [
//    'openid'=>'',
//    'sex'=>'',
//    'province'=>'',
//    'city'=>'',
//    'headimgurl'=>'',
//    'unionid'=>'',
//    'nickname'=>'',
//];