<?php
namespace Maowenke\PersonalPaymentSdk\officialaccount;
use Maowenke\PersonalPaymentSdk\Wecate;

class PublicNumber extends Wecate
{
    protected $url = 'https://api.weixin.qq.com/';

    /**
     * @param false $jump
     * @return false|string
     * @throws \Exception
     */
    public function getCodeUrl($jump=false){
        $bool = $this->verification(['appid','redirect_uri','scope']);
        if($bool===false){
            return false;
        }
        $url = $this->url.'connect/oauth2/authorize?appid='.$this->config['appid'].'&redirect_uri='.urlencode($this->config['redirect_uri']).'&response_type='.($this->config['response_type']??'code').'&scope='.$this->config['scope'].'&state='.($this->config['state']??mt_rand(100,999)).'#wechat_redirect';
        if($jump===false){
            return $url;
        }
        Header("HTTP/1.1 303 See Other");
        Header("Location: $url");
        exit;
    }

    /**通过code获取openid,
     * @param $code
     * @return array|bool|string|openid|access_token
     * @throws \Exception
     */
    public function getOauth2($code){
        $this->config['code'] = $code;
        $bool = $this->verification(['appid','secret']);
        if($bool===false){
            return false;
        }
        $url = $this->url.'sns/oauth2/access_token?appid='.$this->config['appid'].'&secret='.$this->config['secret'].'&code='.$code.'&grant_type=authorization_code';
        $array = $this->curl->http_requests($url,[],[],'GET');
        if(is_array($array)){
            if(isset($array['openid'])){
                $this->config['openid'] = $array['openid'];
            }
            if(isset($array['access_token'])){
                $this->config['access_token'] = $array['access_token'];
            }
            if(isset($array['errcode'])){
                $this->message = $array['errmsg'];
                return false;
            }
            return $array;
        }else{
            $this->message = $array;
            return false;
        }
    }

    /**获取用户详情|scope为 snsapi_userinfo
     * @param string $access_token
     * @param string $openid
     * @param string $code
     * @return bool|mixed|string
     * @throws \Exception
     */
    public function getUserInfo($access_token='',$openid='',$code=''){
        if(empty($access_token)||empty($openid)){
            $bool = $this->verification(['appid','secret']);
            if($bool===false){
                return false;
            }
            if(empty($code)){
                $code = $this->config['code']??false;
                if(empty($code)){
                    $this->message = '没有获取code';
                    return false;
                }
            }
            $array = $this->getOauth2($code);
            if($array===false){
                return false;
            }
            $openid = $array['openid'];
            $access_token = $array['access_token'];
            $url = $this->url.'sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
        }else{
            $bool = $this->verification(['appid','secret']);
            if($bool===false){
                return false;
            }
            $url = $this->url.'sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
        }
        $array = $this->curl->http_requests($url,[],[],'GET');
        if($array===false){
            $this->message = $this->curl->getMessage();
            return false;
        }
        if(isset($array['errcode'])){
            $this->message = $array['errmsg']??'未返回信息';
            return false;
        }
        return $array;
    }

}